package tests;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import tools.Constants;

/**
 * Account_UpdatePassword
 * 		Update Password test
 * @author Tasnim
 */

public class Account_UpdatePassword extends Tests{
	
	private String newPassword = "22newPassword?";
	
	@BeforeTest
	public void beforeTest() throws Exception{

		UserLogin();
	}
	
	@Test
	public void UpdatePassword() throws Exception {
		
		dashboardPage.goToMyAccountPage();
		
		myAccountPage.clickOnUpdatePasswordForm();
		
		myAccountPage.fillUpdatePasswordInfo(password, newPassword);
		myAccountPage.clickOnUpdatePasswordButton();
		
		dashboardPage.clickOnLogout();
		
		loginPage.fillLoginCredentials(username, newPassword);
		
		loginPage.clickOnLoginButton();
		
		System.out.println("\n=== Make sure the new password is working ===\n");
		String message = dashboardPage.getWelcomeLabelContent();
		Thread.sleep(Constants.waitPage);
		
		Assert.assertTrue(message.contains("Welcome"));
		
	}
	
	@AfterTest
	public void resetPassword() throws Exception{
		
		System.out.println("\n=== Reset password to the old one ===\n");
		
		dashboardPage.goToMyAccountPage();
		
		myAccountPage.clickOnUpdatePasswordForm();
		
		myAccountPage.fillUpdatePasswordInfo(password, newPassword);
		myAccountPage.clickOnUpdatePasswordButton();
		
		dashboardPage.clickOnLogout();
	}
	
}
