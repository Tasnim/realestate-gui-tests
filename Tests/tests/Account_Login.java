package tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import tools.Constants;

/**
 * Account_Login
 * 		Login test
 * @author Tasnim
 */

public class Account_Login extends Tests{
	
	@Test
	public void Login() throws Exception {
		
		UserLogin();
		
		String message = dashboardPage.getWelcomeLabelContent();
		Thread.sleep(Constants.waitPage);
		
		Assert.assertTrue(message.contains("Welcome"));
	}
	
}
