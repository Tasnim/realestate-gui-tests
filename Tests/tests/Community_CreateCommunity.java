package tests;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import connect.Community;

/**
 * Community_CreateCommunity
 * 		Create new community test
 * @author Tasnim
 */

public class Community_CreateCommunity extends Tests{
	
	private static String communityName = "by_automated_test";
	
	@BeforeTest
	public void beforeTest() throws Exception{

		UserLogin();
	}
	
	@Test
	public void CreateCommunity() throws Exception {
		
		dashboardPage.goToCommunitiesPage();
		
		communityPage.clickOnCreateCommunityForm();
		
		communityPage.fillCommunityName(communityName);
		
		communityPage.clickOnCreateCommunityButton();
		
		System.out.println("\n=== Make sure that the new community is created ===\n");
		communityPage.clickOnCommunitiesPage();
		
		List<WebElement> communities = communityPage.getCommunitiesList();
		boolean isCreated = false;
		for (WebElement community : communities) {
			if(community.getText().contains(communityName)){
				isCreated = true;
				Assert.assertTrue(isCreated);
				break;
			}
		}
		
		if(!isCreated)
			Assert.assertTrue(isCreated);
		
	}
	
	@AfterTest
	public void deleteCommunity() throws Exception{
		
		System.out.println("\n=== Delete the created community ===\n");
		
		Community.deleteCommunity(communityName);
	}
	
}
