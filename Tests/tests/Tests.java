package tests;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import communities_page.CommunitiesPage;
import dashboard_page.DashboardPage;
import login_page.LoginPage;
import myaccount_page.MyAccountPage;
import tools.Chrome;

public class Tests {
	
	Chrome chrome;
	
	protected static LoginPage loginPage;
	protected static DashboardPage dashboardPage;
	protected static MyAccountPage myAccountPage;
	protected static CommunitiesPage communityPage;
	
	protected static String username = "TestAccount";
	protected static String password = "22TestAccount??";
	
	@BeforeSuite
	public void setUp() throws Exception{
		chrome = new Chrome();
		chrome.open();
		chrome.navigate("http://crowdpowerestate.com/index.html");
		
		loginPage = new LoginPage();
		dashboardPage = new DashboardPage();
		myAccountPage = new MyAccountPage();
		communityPage = new CommunitiesPage();
	}
	
	@AfterSuite
	public void tearDown(){
		chrome.close();
	}
	
	protected void UserLogin() throws Exception{
		
		loginPage.fillLoginCredentials(username, password);
		
		loginPage.clickOnLoginButton();
	}
	
}
