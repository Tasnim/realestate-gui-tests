package connect;

import java.sql.*;

public class Community {

	/**
	 * delete the community from the DB using the RfId based on the community name.
	 * @param communityName :the name of the created community to be deleted
	 * @throws SQLException exception
	 */
	public static void deleteCommunity(String communityName) throws SQLException{
		
		Connection conn = DBconnect.connect();
		Statement stmt = conn.createStatement();  
		
		ResultSet rs = stmt.executeQuery("Select * from Community");
		
		while(rs.next()){
			if(rs.getString("Label").equals(communityName)){
				PreparedStatement preparedStmt = conn.prepareStatement(
						"Delete from Community where RfId = '"+ rs.getString("RfId") + "'");
				preparedStmt.execute();
				
				preparedStmt = conn.prepareStatement(
						"Delete from CommunityAccounts where CommunityRfId = '"+ rs.getString("RfId") + "'");
				preparedStmt.execute();
				
				break;
			}
		}
		conn.close();
	}
	
}
