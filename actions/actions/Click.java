package actions;

import org.openqa.selenium.WebElement;
import org.w3c.dom.Document;

import tools.Constants;
import tools.Driver;

/**
 * Click action on web element
 * 
 * @author Tasnim
 */
public class Click extends Actions{

	public void click(Document doc, String locator, boolean isLocator) throws Exception{
		
		WebElement webElement = null;
		try {
			webElement = waitForElement(doc, locator, isLocator, Constants.waitElement);
			String oldStyle = webElement.getCssValue("background-color");
			highlightElement(Driver.driver, webElement, "");
			webElement.click();
			highlightElement(Driver.driver, webElement, oldStyle);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("fail to click on" + locator);
		}
		
	}
	
}
