package actions;

import org.openqa.selenium.WebElement;
import org.w3c.dom.Document;

import tools.Constants;
import tools.Driver;

/**
 * Type some text value in the web element
 * 
 * @author Tasnim
 */

public class Type extends Actions{

	public void type(Document doc, String locator, boolean isLocator, String value) throws Exception{
		
		WebElement webElement = null;
		try {
			webElement = waitForElement(doc, locator, isLocator, Constants.waitElement);
			String oldStyle = webElement.getCssValue("background-color");
			highlightElement(Driver.driver, webElement, "");
			webElement.sendKeys(value);
			Thread.sleep(Constants.typeTime);
			highlightElement(Driver.driver, webElement, oldStyle);
		} catch (Exception e) {
			throw new Exception("fail to type on " + locator);
		}
		
	}
	
}
