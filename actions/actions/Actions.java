package actions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import tools.Constants;
import tools.Driver;

/**
 * Actions needed for automating web tests.
 * 
 * @author Tasnim
 */
public class Actions {

	/**
	 * get the XPath from the xml tag.
	 * @param doc :XML file of locators
	 * @param locator :XML tag name
	 * @param isLocator :from the XMLL file or passed directly
	 * @return XPath
	 */
	public String getXPath(Document doc, String locator, boolean isLocator){
		if(!isLocator)
			return locator;
		
		NodeList nList = doc.getElementsByTagName(locator);
		Node nNode = nList.item(0);
		Element eElement = (Element) nNode;
		return eElement.getElementsByTagName("locator").item(0).getTextContent();
	}
	
	/**
	 * paint the web element.
	 */
	public void highlightElement(WebDriver driver, WebElement element, String oldStyle) throws InterruptedException {

		JavascriptExecutor js = (JavascriptExecutor) driver;
		
		if(oldStyle.equals("")){
			js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
					"background-color: green;");
			Thread.sleep(Constants.highightTime);
		}
		else
			js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
					"background-color: " + oldStyle + ";");

	}
	
	/**
	 * locate the web element by XPath.
	 */
	public WebElement findElement(String xPath){
		return Driver.driver.findElement(By.xpath(xPath));
	}
	
	/**
	 * locate the web elements by XPath.
	 */
	public List<WebElement> findElements(String xPath){
		return Driver.driver.findElements(By.xpath(xPath));
	}
	
	/**
	 * wait for the web element until it presents.
	 */
	public List<WebElement> waitForElements(Document doc, String locator, boolean isLocator, int timeout) throws Exception {

		long start = System.currentTimeMillis() / 1000;
		long delta = 0;
		long current = 0;
		List<WebElement> webElementsList = null;

		boolean isPresent = false;

		System.out.println("wait for element " + locator);
		
		String xPath = getXPath(doc, locator, isLocator);
		
		System.out.println("timeout = " + timeout + " seconds");

		try {
			while (delta <= timeout) {
				Thread.sleep(850);
				isPresent = false;
				try {
					webElementsList = findElements(xPath);
					for (WebElement webElement : webElementsList) {
						if (webElement.isDisplayed() || webElement.isEnabled() || webElement.getSize().width > 0) {
							isPresent = true;
						}
					}
					
				} catch (Exception ex) {
					isPresent = false;
				}
				
				System.out.println("isElementPresent [" + locator + "] : " + isPresent);

				if (isPresent) {
					break;
				}

				current = System.currentTimeMillis() / 1000;
				delta = (current - start);
				
				System.out.println("wait for elemet 'delta' time :[" + delta + "]");
				System.out.println("wait for elemet 'timeout' :[" + timeout + "]");

				if (delta >= timeout) {
					throw new Exception("wait for webui element fail " + "[" + locator + "]"
							+ "element id [" + xPath + "]");
				}
			}

		} catch (Exception ex) {
			throw new Exception("wait for webui element fail " + "[" + locator + "]"
					+ "element id [" + locator + "]", ex);
		}
		return webElementsList;
	}
	
	/**
	 * wait for the web elements until they're present.
	 */
	public WebElement waitForElement(Document doc, String locator, boolean isLocator, int timeout) throws Exception {

		long start = System.currentTimeMillis() / 1000;
		long delta = 0;
		long current = 0;
		WebElement webElement = null;

		boolean isPresent = false;

		System.out.println("wait for element " + locator);
		
		String xPath = getXPath(doc, locator, isLocator);
		
		System.out.println("timeout = " + timeout + " seconds");

		try {
			while (delta <= timeout) {
				Thread.sleep(850);
				isPresent = false;
				try {
					webElement = findElement(xPath);
					if (webElement.isDisplayed() || webElement.isEnabled() || webElement.getSize().width > 0) {
						isPresent = true;
					}
				} catch (Exception ex) {
					isPresent = false;
				}
				
				System.out.println("isElementPresent [" + locator + "] : " + isPresent);

				if (isPresent) {
					break;
				}

				current = System.currentTimeMillis() / 1000;
				delta = (current - start);
				
				System.out.println("wait for elemet 'delta' time :[" + delta + "]");
				System.out.println("wait for elemet 'timeout' :[" + timeout + "]");

				if (delta >= timeout) {
					throw new Exception("wait for webui element fail " + "[" + locator + "]"
							+ "element id [" + xPath + "]");
				}
			}

		} catch (Exception ex) {
			throw new Exception("wait for webui element fail " + "[" + locator + "]"
					+ "element id [" + locator + "]", ex);
		}
		return webElement;
	}
}
