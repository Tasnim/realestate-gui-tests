package actions;

/**
 * Definition of all web commands actions
 * 
 * @author Tasnim
 */

public class WebCommands {

	public Click clickCommand;
	public Type typeCommand;
	public GetText getTextCommand;
	public GetList getListCommand;
	
	public WebCommands(){
		clickCommand = new Click();
		typeCommand = new Type();
		getTextCommand = new GetText();
		getListCommand = new GetList();
	}
	
}
