package actions;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.w3c.dom.Document;

import tools.Constants;

/**
 * Get the list of items
 * 
 * @author Tasnim
 */

public class GetList extends Actions{

	public List<WebElement> getList(Document doc, String locator, boolean isLocator) throws Exception{
		
		List<WebElement> webElements = null;
		try {
			webElements = waitForElements(doc, locator, isLocator, Constants.waitElement);
			return webElements;
		} catch (Exception e) {
			throw new Exception("fail to get the items list from " + locator);
		}
		
	}
	
}
