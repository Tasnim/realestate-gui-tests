package actions;

import org.openqa.selenium.WebElement;
import org.w3c.dom.Document;

import tools.Constants;
import tools.Driver;

/**
 * Get the text value from the web element
 * 
 * @author Tasnim
 */

public class GetText extends Actions{

	public String getText(Document doc, String locator, boolean isLocator) throws Exception{
		
		WebElement webElement = null;
		String textValue = "";
		try {
			webElement = waitForElement(doc, locator, isLocator, Constants.waitElement);
			String oldStyle = webElement.getCssValue("background-color");
			highlightElement(Driver.driver, webElement, "");
			textValue = webElement.getText();
			highlightElement(Driver.driver, webElement, oldStyle);
			return textValue;
		} catch (Exception e) {
			throw new Exception("fail to get text from " + locator);
		}
		
	}
	
}
