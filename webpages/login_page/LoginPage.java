package login_page;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import actions.WebCommands;
import tools.Constants;

/**
 * LoginPage
 * 		perform actions on the web elements defined in the XML file
 * 
 * @author Tasnim
 */

public class LoginPage{
	
	private static Document doc;
	private static WebCommands webCommand;
	
	public LoginPage() throws ParserConfigurationException, SAXException, IOException{
		webCommand = new WebCommands();
		
		String dir = System.getProperty("user.dir");
		File xmlFile = new File(dir + "/webpages/login_page/loginpage.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		doc = dBuilder.parse(xmlFile);
		doc.getDocumentElement().normalize();
	}
	
	public void fillLoginCredentials(String username, String password) throws Exception{
		System.out.println("\n=== Fill the new login credentials ===\n");
		webCommand.typeCommand.type(doc, "username_field", true, username);
		webCommand.typeCommand.type(doc, "password_field", true, password);
	}
	
	public void clickOnLoginButton() throws Exception{
		System.out.println("\n=== Click on the login button ===\n");
		webCommand.clickCommand.click(doc, "login_button", true);
		Thread.sleep(Constants.waitPage);
	}
	
}
