package myaccount_page;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import actions.WebCommands;
import tools.Constants;

/**
 * MyAccountPage
 * 		perform actions on the web elements defined in the XML file
 * 
 * @author Tasnim
 */

public class MyAccountPage{
	
	private static Document doc;
	private static WebCommands webCommand;
	
	public MyAccountPage() throws ParserConfigurationException, SAXException, IOException{
		webCommand = new WebCommands();
		
		String dir = System.getProperty("user.dir");
		File xmlFile = new File(dir + "/webpages/myaccount_page/myaccountpage.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		doc = dBuilder.parse(xmlFile);
		doc.getDocumentElement().normalize();
	}
	
	public void clickOnUpdatePasswordForm() throws Exception{
		System.out.println("\n=== Click on update password form ===\n");
		webCommand.clickCommand.click(doc, "update_password_form", true);
		Thread.sleep(Constants.waitPage);
	}
	
	public void fillUpdatePasswordInfo(String oldPassword, String newPassword) throws Exception{
		System.out.println("\n=== Fill the new password to be updated ===\n");
		webCommand.typeCommand.type(doc, "old_password_field", true, oldPassword);
		webCommand.typeCommand.type(doc, "new_password_field", true, newPassword);
		webCommand.typeCommand.type(doc, "reenter_password_field", true, newPassword);
	}
	
	public void clickOnUpdatePasswordButton() throws Exception{
		System.out.println("\n=== Click on Update Password button ===\n");
		webCommand.clickCommand.click(doc, "update_password_button", true);
		Thread.sleep(Constants.waitPage);
	}
	
}
