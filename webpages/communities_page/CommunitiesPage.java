package communities_page;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.WebElement;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import actions.WebCommands;
import tools.Constants;

/**
 * CommunitiesPage
 * 		perform actions on the web elements defined in the XML file
 * 
 * @author Tasnim
 */

public class CommunitiesPage{
	
	private static Document doc;
	private static WebCommands webCommand;
	
	public CommunitiesPage() throws ParserConfigurationException, SAXException, IOException{
		webCommand = new WebCommands();
		
		String dir = System.getProperty("user.dir");
		File xmlFile = new File(dir + "/webpages/communities_page/communitiespage.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		doc = dBuilder.parse(xmlFile);
		doc.getDocumentElement().normalize();
	}
	
	public void clickOnCommunitiesPage() throws Exception{
		System.out.println("\n=== Click on communities page ===\n");
		webCommand.clickCommand.click(doc, "created_communities_page", true);
		Thread.sleep(Constants.waitPage);
	}
	
	public void clickOnCreateCommunityForm() throws Exception{
		System.out.println("\n=== Click on create community form ===\n");
		webCommand.clickCommand.click(doc, "create_community_form", true);
		Thread.sleep(Constants.waitPage);
	}
	
	public void fillCommunityName(String communityName) throws Exception{
		System.out.println("\n=== Fill the community name ===\n");
		webCommand.typeCommand.type(doc, "community_label_field", true, communityName);
	}
	
	public void clickOnCreateCommunityButton() throws Exception{
		System.out.println("\n=== Click on create community button ===\n");
		webCommand.clickCommand.click(doc, "create_community_button", true);
		Thread.sleep(Constants.waitPage);
	}
	
	public List<WebElement> getCommunitiesList() throws Exception{
		System.out.println("\n=== Retrieve the created communities list ===\n");
		List<WebElement> list = webCommand.getListCommand.getList(doc, "created_communities_list", true);
		Thread.sleep(Constants.waitPage);
		return list;
	}
}
