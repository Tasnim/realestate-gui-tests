package dashboard_page;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import actions.WebCommands;
import tools.Constants;

/**
 * DashboardPage
 * 		perform actions on the web elements defined in the XML file
 * 
 * @author Tasnim
 */

public class DashboardPage{
	
	private static Document doc;
	private static WebCommands webCommand;
	
	public DashboardPage() throws ParserConfigurationException, SAXException, IOException{
		webCommand = new WebCommands();
		
		String dir = System.getProperty("user.dir");
		File xmlFile = new File(dir + "/webpages/dashboard_page/dashboardpage.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		doc = dBuilder.parse(xmlFile);
		doc.getDocumentElement().normalize();
	}
	
	public String getWelcomeLabelContent() throws Exception{
		System.out.println("\n=== Check the welcome message ===\n");
		return webCommand.getTextCommand.getText(doc, "welcome_msg", true);
	}
	
	public void goToCommunitiesPage() throws Exception{
		System.out.println("\n=== Go to Communities page ===\n");
		webCommand.clickCommand.click(doc, "menu_list", true);
		webCommand.clickCommand.click(doc, "communities_page", true);
		Thread.sleep(Constants.waitPage);
	}
	
	public void goToMyAccountPage() throws Exception{
		System.out.println("\n=== Go to My Account page ===\n");
		webCommand.clickCommand.click(doc, "menu_list", true);
		webCommand.clickCommand.click(doc, "myaccount_page", true);
		Thread.sleep(Constants.waitPage);
	}
	
	public void clickOnLogout() throws Exception{
		System.out.println("\n=== Logout ===\n");
		webCommand.clickCommand.click(doc, "menu_list", true);
		webCommand.clickCommand.click(doc, "logout_button", true);
		Thread.sleep(Constants.waitPage);
	}
	
}
