package tools;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

/**
 * Chrome browser actions needed to run the tests
 * 
 * @author Tasnim
 */

public class Chrome {
	
	public Chrome(){
		String home = System.getProperty("user.dir");
		System.setProperty("webdriver.chrome.driver", home + "\\libs\\chromedriver.exe");
	}
	
	public void open(){
		try{
			Driver.driver = new ChromeDriver();
		} catch(Exception e){
			ChromeOptions options = new ChromeOptions();
			options.setBinary("E:\\New Installed\\Google\\Chrome\\Application\\Chrome.exe");
			Driver.driver = new ChromeDriver(options);
		}
		Driver.driver.manage().window().maximize();
		Driver.driver.manage().deleteAllCookies();
	}
	
	public void close(){
		Driver.driver.quit();
	}
	
	public void navigate(String url){
		Driver.driver.navigate().to(url);
	}
	
}
