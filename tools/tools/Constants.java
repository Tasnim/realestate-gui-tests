package tools;

/**
 * Timeout constants
 * 
 * @author Tasnim
 */

public class Constants {

	public static int waitElement = 100;
	public static int highightTime = 1200;
	public static int typeTime = 1000;
	public static int waitPage = 2000;
	
}
